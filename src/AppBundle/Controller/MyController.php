<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MyController extends Controller
{
    public function listar($nombreEntidad)
    {
        $entidades = $this->getDoctrine()->getRepository($nombreEntidad)->findAll();

        return array(
            'entidades' => $entidades
        );
    }
}

<?php

namespace AppBundle\DataFixture\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Alimento;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadAlimentoData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function creaAlimento(ObjectManager $manager, $nombre)
    {
        $alimento = new Alimento();
        $alimento->setNombre($nombre);
        $alimento->setTipo($this->getReference('tipo-defecto'));
        $alimento->setImageName('001.jpeg');

        $manager->persist($alimento);
    }

    public function load(ObjectManager $manager)
    {
        $this->creaAlimento(
            $manager, 'Alimento de prueba');

        $this->creaAlimento(
            $manager, 'Alimento de prueba 2');

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}
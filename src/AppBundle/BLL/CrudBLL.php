<?php

namespace AppBundle\BLL;

use Doctrine\ORM\EntityManager;

class CrudBLL
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function listar($nombreEntidad)
    {
        $entidades = $this->em->getRepository($nombreEntidad)->findAll();

        return array(
            'entidades' => $entidades
        );
    }
}
<?php


namespace AppBundle\BLL;

class UtilFecha
{
    public function utilfecha_datetimeToString(\DateTime $fecha)
    {
        return $fecha->format("d/m/Y H:i");
    }
}
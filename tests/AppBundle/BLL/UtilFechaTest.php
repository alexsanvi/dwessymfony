<?php

namespace AppBundle\Tests\Controller;

use AppBundle\BLL\UtilFecha;

class UtilFechaTest extends \PHPUnit_Framework_TestCase
{
    public function test_utilfecha_datetimeToString()
    {
        $utilFecha = new UtilFecha();

        $fecha = new \DateTime('2000-01-01 10:10');
        $fecha_string = $utilFecha->utilfecha_datetimeToString($fecha);

        $this->assertEquals("01/01/2000 10:10", $fecha_string);
    }
}

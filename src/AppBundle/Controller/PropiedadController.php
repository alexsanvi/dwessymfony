<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Propiedad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Propiedad controller.
 *
 * @Route("propiedades")
 */
class PropiedadController extends MyController
{
    /**
     * Lists all propiedad entities.
     *
     * @Route("/", name="propiedades_index")
     * @Template(":propiedad:index.html.twig")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->listar('AppBundle:Propiedad');
    }

    /**
     * Creates a new propiedad entity.
     *
     * @Route("/new", name="propiedades_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $propiedad = new Propiedad();
        $form = $this->createForm('AppBundle\Form\PropiedadType', $propiedad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($propiedad);
            $em->flush($propiedad);

            return $this->redirectToRoute('propiedades_show', array('id' => $propiedad->getId()));
        }

        return $this->render('propiedad/new.html.twig', array(
            'propiedad' => $propiedad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a propiedad entity.
     *
     * @Route("/{id}", name="propiedades_show")
     * @Method("GET")
     */
    public function showAction(Propiedad $propiedad)
    {
        $deleteForm = $this->createDeleteForm($propiedad);

        return $this->render('propiedad/show.html.twig', array(
            'propiedad' => $propiedad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing propiedad entity.
     *
     * @Route("/{id}/edit", name="propiedades_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Propiedad $propiedad)
    {
        $deleteForm = $this->createDeleteForm($propiedad);
        $editForm = $this->createForm('AppBundle\Form\PropiedadType', $propiedad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('propiedades_edit', array('id' => $propiedad->getId()));
        }

        return $this->render('propiedad/edit.html.twig', array(
            'propiedad' => $propiedad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a propiedad entity.
     *
     * @Route("/{id}", name="propiedades_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Propiedad $propiedad)
    {
        $form = $this->createDeleteForm($propiedad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($propiedad);
            $em->flush($propiedad);
        }

        return $this->redirectToRoute('propiedades_index');
    }

    /**
     * Creates a form to delete a propiedad entity.
     *
     * @param Propiedad $propiedad The propiedad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Propiedad $propiedad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('propiedades_delete', array('id' => $propiedad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

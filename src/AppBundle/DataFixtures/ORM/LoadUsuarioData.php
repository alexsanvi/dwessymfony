<?php

namespace AppBundle\DataFixture\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Usuario;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsuarioData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
/*
        $userAdmin = new Usuario();
        $userAdmin->setNombre('Prueba');
        $userAdmin->setApellidos($this->container->getParameter('kernel.root_dir'));
        $userAdmin->setUsername('admin2');
        $userAdmin->setSalt(md5(uniqid()));

        // the 'security.password_encoder' service requires Symfony 2.6 or higher
        $encoder = $this->container->get('security.password_encoder');

        $passwordSinEncriptar = 'admin';
        $password = $encoder->encodePassword($userAdmin, $passwordSinEncriptar);
        $userAdmin->setPassword($password);

        $manager->persist($userAdmin);
        $manager->flush();
*/
    }

    public function getOrder()
    {
        return 1;
    }
}
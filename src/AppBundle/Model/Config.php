<?php
namespace AppBundle\Model;

class Config
{
    static public $mvc_bd_hostname = "localhost";
    static public $mvc_bd_nombre = "alimentos";
    static public $mvc_bd_usuario = "usualimentos";
    static public $mvc_bd_clave = "usualimentos";
    static public $mvc_vis_css = "estilo.css";
}
<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AlimentoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nombre', TextType::class)
            ->add('energia', TextType::class)
            ->add('proteina', TextType::class)
            ->add('hidratocarbono', TextType::class)
            ->add('fibra', TextType::class)
            ->add('grasatotal', TextType::class)
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ])
            ->add('tipo', EntityType::class, array(
                'class' => 'AppBundle:Tipo',
                'choice_label' => 'nombre'))
            ->add('insertar', SubmitType::class, array('label'=>'Guardar'));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Alimento'
        ));
    }

    public function getName()
    {
        return 'app_bundle_alimento_type';
    }
}

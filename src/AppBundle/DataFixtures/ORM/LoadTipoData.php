<?php

namespace AppBundle\DataFixture\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Tipo;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTipoData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function creaTipo(ObjectManager $manager, $nombre, $descripcion)
    {
        $tipo = new Tipo();
        $tipo->setNombre($nombre);
        $tipo->setDescripcion($descripcion);

        $manager->persist($tipo);

        return $tipo;
    }

    public function load(ObjectManager $manager)
    {
        $tipo = $this->creaTipo(
            $manager, 'Tipo de prueba', 'Este es un tipo de prueba');

        $tipo2 = $this->creaTipo(
            $manager, 'Tipo de prueba 2', 'Este es un tipo de prueba 2');

        $manager->flush();

        $this->addReference('tipo-defecto', $tipo);
    }

    public function getOrder()
    {
        return 10;
    }
}
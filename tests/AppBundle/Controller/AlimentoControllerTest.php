<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlimentoControllerTest extends WebTestCase
{
    public function testListarAlimentosAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/alimentos/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            1,
            $crawler->filter('table')->count());
    }
}

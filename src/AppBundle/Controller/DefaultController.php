<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $parameters = array(
            'mensaje' => 'Bienvenido a la aplicación de alimentos',
            'fecha' => date('d-m-y'),
            'user' => $this->getUser());

        return $this->render('AppBundle:default:index.html.twig', $parameters);
    }
}

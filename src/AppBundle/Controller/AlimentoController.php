<?php


namespace AppBundle\Controller;

use AppBundle\Entity\Alimento;
use AppBundle\Form\AlimentoType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\Model;
use AppBundle\Model\Config;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/alimentos")
 */
class AlimentoController extends Controller
{
    /**
     * @Route("/", name="alimentos_listar")
     * @Template("AppBundle:alimento:listar.html.twig")
     * @Method({"GET"})
     */
    public function listarAction()
    {
        return $this->get('app.bll.crud')->listar('AppBundle:Alimento');
    }


    /**
     * @Route(  "/{id}",
     *          name="alimentos_ver",
     *          requirements={"id": "\d+"})
     * @Template("AppBundle:alimento:ver.html.twig")
     * @Method({"GET"})
     * @Security("has_role('ROLE_USER')")
     */
    public function verAlimentoAction(Request $request, $id)
    {
//        $this->denyAccessUnlessGranted('ROLE_USER');

        $alimento = $this->getDoctrine()->getRepository('AppBundle:Alimento')->find($id);
        if(!$alimento)
            throw new NotFoundHttpException("Alimento no encontrado");

        return array(
            'alimento' => $alimento
        );
    }

    /**
     * @Route("/new", name="alimentos_insertar")
     * @Template("AppBundle:alimento:formInsertar.html.twig")
     * @Method({"GET", "POST"})
     */
    public function insertarAlimentoAction(Request $request)
    {
        $alimento = new Alimento();

        $form = $this->createForm(AlimentoType::class, $alimento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $alimento = $form->getData();

            $em->persist($alimento);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'notice',
                'Se ha insertado un nuevo alimento '.$alimento->getId()
            );

            return $this->redirect(
                $this->generateUrl('alimentos_listar'));
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/edit/{id}", name="alimentos_edit")
     * @Template("AppBundle:alimento:formInsertar.html.twig")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Alimento $alimento)
    {
        $editForm = $this->createForm('AppBundle\Form\AlimentoType', $alimento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect(
                $this->generateUrl('alimentos_listar'));
        }

        return array(
            'alimento' => $alimento,
            'form' => $editForm->createView()
        );
    }

    /**
     * @Route("/eliminar/{id}",
     *          name="alimentos_eliminar",
     *          requirements={"id": "\d+"})
     * @Method({"GET"})
     */
    public function eliminarAction(Request $request, $id)
    {
        $alimento = $this->getDoctrine()
            ->getRepository('AppBundle:Alimento')
            ->find($id);
        if(!$alimento)
            throw new NotFoundHttpException("Alimento no encontrado");

        $em = $this->getDoctrine()->getManager();
        $em->remove($alimento);
        $em->flush();

        return $this->redirect(
            $this->generateUrl('alimentos_listar'));
    }

    /**
     * @Route("/buscar", name="alimentos_buscar")
     * @Template("AppBundle:alimento:buscar.html.twig")
     * @Method({"POST", "GET"})
     */
    public function buscarAction(Request $request)
    {
        $params = array(
            'nombre' => '',
            'energia' => '',
            'alimentos' => array()
        );

        if ($request->server->get('REQUEST_METHOD') == 'POST')
        {
            $params_busqueda = array();
            $params['nombre'] = $request->request->get('nombre');
            $params['energia'] = $request->request->get('energia');

            if (isset($params['nombre']) && $params['nombre'] !== '')
                $params_busqueda['nombre'] = $params['nombre'];
            if (isset($params['energia']) && $params['energia'] !== '')
                $params_busqueda['energia'] = $params['energia'];

            $params['alimentos'] = $this->getDoctrine()
                ->getRepository('AppBundle:Alimento')
                ->findWithFilter($params_busqueda);
        }

        return $params;
    }
}
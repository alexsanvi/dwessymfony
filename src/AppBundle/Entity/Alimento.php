<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Alimento
 *
 * @ORM\Table(name="alimento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlimentoRepository")
 * @Vich\Uploadable
 */
class Alimento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     * @Assert\NotBlank(
     *      message = "El campo nombre no puede quedarse vacío"
     * )
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="energia", type="integer")
     * @Assert\NotBlank(
     *      message = "El campo nombre no puede quedarse vacío"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 500,
     *      minMessage = "La energía tiene que ser de al menos {{ limit }}",
     *      maxMessage = "La energía máxima es {{ limit }}",
     *      invalidMessage= "La energía total tiene que ser un número"
     * )
     */
    private $energia;

    /**
     * @var int
     *
     * @ORM\Column(name="proteina", type="integer")
     * @Assert\NotBlank(
     *      message = "El campo nombre no puede quedarse vacío"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 500,
     *      minMessage = "La proteina tiene que ser de al menos {{ limit }}",
     *      maxMessage = "La proteina máxima es {{ limit }}",
     *      invalidMessage= "La proteina total tiene que ser un número"
     * )
     */
    private $proteina;

    /**
     * @var int
     *
     * @ORM\Column(name="hidratocarbono", type="integer")
     * @Assert\NotBlank(
     *      message = "El campo nombre no puede quedarse vacío"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 500,
     *      minMessage = "El hidrato de carbono tiene que ser de al menos {{ limit }}",
     *      maxMessage = "El hidrato de carbono máxima es {{ limit }}",
     *      invalidMessage= "El hidrato de carbono tiene que ser un número"
     * )
     */
    private $hidratocarbono;

    /**
     * @var int
     *
     * @ORM\Column(name="fibra", type="integer")
     * @Assert\NotBlank(
     *      message = "El campo nombre no puede quedarse vacío"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 500,
     *      minMessage = "La fibra tiene que ser de al menos {{ limit }}",
     *      maxMessage = "La fibra máxima es {{ limit }}",
     *      invalidMessage= "La fibra tiene que ser un número"
     * )
     */
    private $fibra;

    /**
     * @var int
     *
     * @ORM\Column(name="grasatotal", type="integer")
     * @Assert\NotBlank(
     *      message = "El campo nombre no puede quedarse vacío"
     * )
     * @Assert\Range(
     *      min = 0,
     *      max = 500,
     *      minMessage = "La grasa total tiene que ser de al menos {{ limit }}",
     *      maxMessage = "La grasa total máxima es {{ limit }}",
     *      invalidMessage= "La grasa total tiene que ser un número"
     * )
     */
    private $grasatotal;

    /**
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @Assert\NotBlank(
     *      message = "El campo tipo no puede quedarse vacío"
     * )
     */
    private $tipo;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="alimento_image", fileNameProperty="imageName")
     *
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->propiedades = new ArrayCollection();
        $this->energia = 10;
        $this->hidratocarbono = 10;
        $this->fibra = 10;
        $this->grasatotal = 10;
        $this->proteina = 10;
        $this->updatedAt = new \DateTime('now');
    }

    function __toString()
    {
        return $this->nombre;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Alimento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set energia
     *
     * @param integer $energia
     *
     * @return Alimento
     */
    public function setEnergia($energia)
    {
        $this->energia = $energia;

        return $this;
    }

    /**
     * Get energia
     *
     * @return int
     */
    public function getEnergia()
    {
        return $this->energia;
    }

    /**
     * Set proteina
     *
     * @param integer $proteina
     *
     * @return Alimento
     */
    public function setProteina($proteina)
    {
        $this->proteina = $proteina;

        return $this;
    }

    /**
     * Get proteina
     *
     * @return int
     */
    public function getProteina()
    {
        return $this->proteina;
    }

    /**
     * Set hidratocarbono
     *
     * @param integer $hidratocarbono
     *
     * @return Alimento
     */
    public function setHidratocarbono($hidratocarbono)
    {
        $this->hidratocarbono = $hidratocarbono;

        return $this;
    }

    /**
     * Get hidratocarbono
     *
     * @return int
     */
    public function getHidratocarbono()
    {
        return $this->hidratocarbono;
    }

    /**
     * Set fibra
     *
     * @param integer $fibra
     *
     * @return Alimento
     */
    public function setFibra($fibra)
    {
        $this->fibra = $fibra;

        return $this;
    }

    /**
     * Get fibra
     *
     * @return int
     */
    public function getFibra()
    {
        return $this->fibra;
    }

    /**
     * Set grasatotal
     *
     * @param integer $grasatotal
     *
     * @return Alimento
     */
    public function setGrasatotal($grasatotal)
    {
        $this->grasatotal = $grasatotal;

        return $this;
    }

    /**
     * Get grasatotal
     *
     * @return int
     */
    public function getGrasatotal()
    {
        return $this->grasatotal;
    }

    /**
     * Set tipo
     *
     * @param \AppBundle\Entity\Tipo $tipo
     *
     * @return Alimento
     */
    public function setTipo(\AppBundle\Entity\Tipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \AppBundle\Entity\Tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add propiedade
     *
     * @param \AppBundle\Entity\Propiedad $propiedad
     *
     * @return Alimento
     */
    public function addPropiedad(Propiedad $propiedade)
    {
        $this->propiedades[] = $propiedade;

        return $this;
    }

    /**
     * Remove propiedade
     *
     * @param \AppBundle\Entity\Propiedad $propiedad
     */
    public function removePropiedad(Propiedad $propiedade)
    {
        $this->propiedades->removeElement($propiedade);
    }

    /**
     * Get propiedades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropiedades()
    {
        return $this->propiedades;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getImageName()
    {
        return $this->imageName;
    }
}
